# -*- coding: utf-8 -*-
"""Create an application instance."""
from libre_space_tools.app import create_app

app = create_app()
